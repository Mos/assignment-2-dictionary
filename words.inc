%include "list_config.inc"

%macro add_key 1
%if key_size = 8
    db %1, 0
%elif key_size = 16
    dw %1, 0
%else
    dd %1, 0
%endif
%endmacro

global list_start
global list_end
    
list_start:

colon "Bonjour", key_1
add_key "Hello"

colon "J'aime le café", key_2
add_key "I like coffee"

colon "Ravi de vous rencontrer", key_3
add_key "Nice to meet you"

list_end: